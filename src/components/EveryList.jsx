import React from 'react'
import CardsDisplay from './CardsDisplay'

const EveryList = ({ listArray, handleArchieve }) => {
    return (
  <>
        { listArray && listArray.map((list, index) =>
            <div key={index}>
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">{list.name}</h5>
                        <div >
                            <CardsDisplay id={list.id} />
                        </div>
                        <button className='mt-5 btn btn-warning' onClick={() => handleArchieve(list.id)}>Archive</button>
                    </div>
                </div>

            </div>
            )}
        </>
  )
}

export default EveryList 