import React, { useEffect, useState } from 'react'
import { deleteChecklist, getChecklists } from '../services/ChecklistDisplayAPI'
import ItemDisplay from './ItemDisplay'

const ChecklistDisplay = ({cardId}) => {

const [checklistData,setChecklistData]=useState([])

    useEffect(() => {
    getChecklists(cardId).then(response => setChecklistData(response))
    }, [cardId])    
    
    let handleDelete = (checklistId) => {
        deleteChecklist(checklistId).then(() => setChecklistData(checklistData.filter( everyCheck=> checklistId !== everyCheck.id)))
    }
  return (
      <div>
          {checklistData.length > 0 && (checklistData.map((everyCheck, index) => {
              return (
                  <div key={index}>
                      <div>
                          <h1>{everyCheck.name} <button onClick={() => handleDelete(everyCheck.id)}>✘</button></h1>
                          
                          </div>
                      <ItemDisplay checkId={ everyCheck.id} />
                  </div>
              )
          }))}
    </div>
  )
}

export default ChecklistDisplay