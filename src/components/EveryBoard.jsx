import React from 'react'
import { Link } from 'react-router-dom'

let imageUrl = "https://trello-backgrounds.s3.amazonaws.com/SharedBackground/480x320/fde291621e297defb5fe147745217e73/photo-1680761060530-87f01f1f1803.jpg"


const EveryBoard = ({ boardData, handleDelete }) => {
    return (
    <>
    {
        boardData.length>0 &&
            (boardData.map((board, index) => {
            return (
                <div className="card" key={board.id + index} style={{ width: "18rem" }}>
                    <Link to={"/board/" + board.id} >
                        <img src={imageUrl} className="card-img-top p-2" alt="..." />
                    </Link>
                    <div className="card-img">
                        <h5 className="card-title m-2">{board.name}</h5>
                    </div>
                    <div className="card-body">
                        {/* <button type='button' className='btn btn-danger' onClick={() => handleDelete(board.id)}>delete</button> */}
                    </div>
                </div>

            )
        })
        )
            }
        </>
    )
}

export default EveryBoard