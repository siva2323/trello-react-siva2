import React from 'react'
import { Link } from 'react-router-dom'
import { Box, Flex } from "@chakra-ui/react"

let logoUrl = "https://wac-cdn-2.atlassian.com/image/upload/f_auto,q_auto/dam/jcr:714e5454-59af-4f8c-a205-3fc5b6722733"

const Navbar = () => {
  return (
    <div className='d-flex'>
      {/* <Link to="/"><button className='btn btn-primary'>BACK TO HOME</button></Link>
          <Box border='1px' borderColor='gray.200'>
        <img src={logoUrl} width="200%" height={32} />
      </Box> */}
      <nav className="navbar navbar-expand-lg bg-body-tertiary">
        <div className="container-fluid">
          <Link to="/" ><img src={logoUrl} width="200%" height={32} /></Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <a className="nav-link " aria-current="page" href="#">
                  Workspaces
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Recent
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Starred
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

    </div>
  )
}

export default Navbar

  