import React from 'react'
import { archieveList, createList, getLists } from '../services/ListDisplayAPI'
import { useState, useEffect } from 'react';
import { useParams } from 'react-router'
import EveryList from './EveryList';

const ListDisplay = () => {

  const { boardId } = useParams();

  const [listArray, setListArray] = useState([])
  const [query, setQuery] = useState("")

  useEffect(() => {
    getLists(boardId).then(response => setListArray(response))
  }, [])

  let handleChange = (event) => {
    setQuery(event.target.value)
  }

  let handleSubmit = (event) => {
    event.preventDefault()
    createList(boardId, query).then(response => setListArray([...listArray, response]))
    setQuery("")
  }

  let handleArchieve = (id) => {
    archieveList(id).then(() => {
      setListArray(listArray.filter(everyList => everyList.id !== id))
    })
  }

  return (
    <div >
      <div className='d-flex overflow-auto'>

        <EveryList listArray={listArray} handleArchieve={handleArchieve} />

        <form onSubmit={handleSubmit}>
          <input type="text" className='mx-5 mt-4 h4 pb-2 mb-4 border border-5 black' placeholder='Add a list'  onChange={handleChange} value={query} />
        </form>
      </div>
    </div>
  )
}

export default ListDisplay