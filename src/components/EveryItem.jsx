import React from 'react'

const EveryItem = ({ itemData, handleDelete }) => {
    return (
        <div>
            {itemData.map((item,index) => {
                return (
                    <div  key={index} className='d-flex flex-wrap'>
                        <div className="form-check">
                            <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                            <label className="form-check-label" htmlFor="flexCheckDefault">
                                <p>{item.name}</p>
                            </label>
                        <button onClick={() => handleDelete(item.id)}>✘</button>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default EveryItem