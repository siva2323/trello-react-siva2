import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Button,
    FormControl,
    FormLabel,
    Input
} from '@chakra-ui/react'
import React, { useState } from 'react'
import { useDisclosure } from '@chakra-ui/react'
import ChecklistDisplay from './ChecklistDisplay'
import { createChecklist } from '../services/ChecklistDisplayAPI'

export default function InitialFocus({cardName,cardId}) {
          
         const {isOpen, onOpen, onClose} = useDisclosure()
    const [query, setQuery] = useState("")
    const [checklistName,setChecklistName]=useState([])
          const initialRef = React.useRef(null)
    const finalRef = React.useRef(null)
    

    let handleChange=(event) =>{
        setQuery(event.target.value)        
    }
    let handleSubmit = (cardId) => {
        createChecklist(cardId,query)
            .then(response => setChecklistName([...checklistName, response]))
        setQuery("")
    }

          return (
          <>
                  <Button onClick={onOpen}>{ cardName}</Button>

              <Modal
                  initialFocusRef={initialRef}
                  finalFocusRef={finalRef}
                  isOpen={isOpen}
                  onClose={onClose}
              >
                  <ModalOverlay />
                  <ModalContent>
                      <ModalHeader>Create new checklist</ModalHeader>
                      <ModalCloseButton />
                      <ModalBody pb={6}>
                              <FormControl >
                                  <FormLabel  >checklist Title</FormLabel>
                                  <Input ref={initialRef} placeholder='new checklist name' value={query} onChange={handleChange} />
                              </FormControl>
                              <ChecklistDisplay cardId={cardId} />
                      </ModalBody>

                      <ModalFooter>
                          <Button colorScheme='blue' mr={3} onClick={() => handleSubmit(cardId)} >
                              Create
                          </Button>
                          <Button onClick={onClose}>Cancel</Button>
                          
                      </ModalFooter>
                  </ModalContent>
              </Modal>
          </>
          )
}


