import React, { useState,useEffect } from 'react'
import { getItems } from '../services/ItemDisplayAPI'
import EveryItem from './EveryItem'
import { deleteItem } from '../services/ItemDisplayAPI'
import { createItem } from '../services/ItemDisplayAPI'

const ItemDisplay = ({ checkId }) => {
  
  const [itemData,setItemData]=useState([])
const [query,setQuery] =useState("")

  
  useEffect(() => {
    getItems(checkId).then(response => setItemData(response))
  },[checkId])
  
  let handleChange = (event) => {
    setQuery(event.target.value)
  }

  let handleClick=()=>{
    createItem(checkId, query).then(response => setItemData([...itemData, response]))
    setQuery("")
  }
  let handleDelete = (itemId) => {
    deleteItem(checkId,itemId).then(() => setItemData(itemData.filter(item => item.id !== itemId)))
  }
  
  return (
    <div>
      {itemData.length > 0 && <EveryItem itemData={itemData} handleDelete={ handleDelete} />}
      <div className="input-group mb-3">
        <input type="text" className="form-control" placeholder="Add an item" onChange={handleChange} aria-label="Recipient's username" value={query} aria-describedby="button-addon2"/>
          <button className="btn btn-outline-secondary" type="button" onClick={handleClick} id="button-addon2">Add</button>
      </div>


    </div>
  )
}

export default ItemDisplay