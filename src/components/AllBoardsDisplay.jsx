import React, { useEffect, useState } from 'react';
import { createBoard, getBoards, deleteBoard } from "../services/AllBoardsDisplayAPI"
import EveryBoard from './EveryBoard';



const AllBoards = () => {
    const [boardData, setBoardData] = useState([]);
    const [query, setQuery] = useState("")

    useEffect(() => {
        getBoards().then(response => setBoardData(response))
    }, [])


    let  handleSubmit=(event) => {
        event.preventDefault()
        if (query.length > 0) {
            createBoard(query).then(response => setBoardData([...boardData, response]))
        }
        setQuery("")
    }

    let handleChange=(event)=> {
        setQuery(event.target.value)
    }

    let handleDelete=(id)=> {
        deleteBoard(id).then(() => setBoardData(boardData.filter(board => board.id !== id)))
    }

    return (

        <div>
            <h3 className='mx-5 d-flex justify-content-center'>Your boards</h3>
            <div className='d-flex flex-wrap mx-5 px-5 grid gap-5 column-gap-5 '  >
                <div className='d-flex flex-wrap mx-5 px-5 grid gap-5 column-gap-5' >
                <EveryBoard boardData={boardData} handleDelete={handleDelete} />
                <form onSubmit={handleSubmit}>
                    <input type="text" className='mx-5 mt-4 h4 pb-2 mb-4 border border-5 black img-thumbnail w-75' placeholder='Create a board'  onChange={handleChange} value={query} />
                </form>
                </div >
            </div>
        </div>
    )
}

export default AllBoards

