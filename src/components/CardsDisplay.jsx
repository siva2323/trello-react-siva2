import { createCard, deleteCard, getCards } from '../services/CardsDisplayAPI'
import React, { useEffect, useState } from 'react'
import { Wrap, WrapItem } from '@chakra-ui/react'
import { Input,Button } from '@chakra-ui/react'
import EveryCard from './EveryCard'

const CardsDisplay = ({ id }) => {
    const [cardsArray, setCardsArray] = useState([])
    const [query,setQuery]=useState("")



    useEffect(() => {
        getCards(id).then(response => setCardsArray(response))
    }, [id])

    let handleDeleteCard=(id)=> {
        deleteCard(id).then(() => setCardsArray(cardsArray.filter(card => card.id!==id)))
    }

    let handleSubmit=(event)=> {
        event.preventDefault()
        createCard(id, query).then(response=> setCardsArray([...cardsArray,response]))
        setQuery("")
    }

let handleChange=(event)=> {
        setQuery(event.target.value)
    }

    return (
        <div>
            {cardsArray.length > 0 &&
                cardsArray.map((card, index) => {
                    return (
                        <div className='mt-3 border border-dark p-1' key={index + card.name} >
                            <Wrap spacing='30px' align='center' >
                                <WrapItem>
                                    <EveryCard cardName={ card.name} cardId={card.id} />
                                </WrapItem>
                                    <button className='btn btn-danger' onClick={() => handleDeleteCard(card.id)}>delete</button>
                            </Wrap>
                        </div>
                    )
                })
            }
            <form onSubmit={handleSubmit}>
                <Input placeholder='new card ' onChange={handleChange} value={query} />
                <Button onClick={handleSubmit} colorScheme='blue'>Create Card</Button>
            </form>
            
        </div>
    )
}

export default CardsDisplay