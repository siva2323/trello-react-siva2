import Navbar from "./components/Navbar"
import AllBoardsDisplay from "./components/AllBoardsDisplay"
import { Routes,Route,BrowserRouter } from "react-router-dom"
import ListDisplay from "./components/ListDisplay"
import Error from "./components/Error"

function App() {

  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<AllBoardsDisplay />} />
          <Route path="/board/:boardId" element={<ListDisplay />} />
          <Route path="*" element={<Error />} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
