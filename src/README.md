# Trello Clone

## Hosted link

- Hosted link - https://scintillating-marigold-af28cd.netlify.app/

## Instructions

Build a trello clone using the official Trello API. The following user stories have to implemented:
Multiple boards -> multiple lists -> and so on, such that:

- All boards view: URL is /boards
- Single board view which displays all lists in that board: URL is /boards/<board_id>

This is called client side routing. This should be implemented using the library react-router.

Requirements:

- Use one CSS framework like bootstrap
- The API requests have to implemented using axios
- Repo name format: trello-react-<first_name>
https://www.youtube.com/watch?v=db_y-sEA1ps 
Boards
- Display Boards
- Create new board

Lists
- Display all lists in a board
- Create a new list
- Delete/Archive a list

Cards
- Display all cards in a list
- Create a new card in a list
- Delete a card

Checklists
- Display all checklists in a card
- Create a checklist in a card
- Delete a checklist

Checkitems
- Display all checkitems
- Create a new checkitem in a checklist
- Delete a checkitem
- Check a checkitem
- Uncheck a checkitem
And make sure to test your app as you are building it.