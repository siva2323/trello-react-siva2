import axios from "axios"
const APIKey = "512b806d37d0b5db167074f631eed3e7"
const APIToken = "ATTAbeec5eaa8a7337fab5d2c505dafe1d14c531be3c8247e6802a8ddc185955220cD98D4C3B"



export function getBoards() {
    return axios.get(`https://api.trello.com/1/members/me/boards?key=${APIKey}&token=${APIToken}`)
        .then(response => response.data)
        .catch(error => console.log("Error occured in fetching API" + error))
}


export function deleteBoard(id) {
    return axios.delete(`https://api.trello.com/1/boards/${id}?key=${APIKey}&token=${APIToken}`)
        .catch(error => console.log("Error occured in deleting board" + error))
}

export function createBoard(newBoardName) {
    return axios.post(`https://api.trello.com/1/boards/?name=${newBoardName}&key=${APIKey}&token=${APIToken}`).then(response => response.data)
        .catch(error => console.log("Error occured in creating board" + newBoardName + error))
}
