import axios from "axios"

const APIKey = "512b806d37d0b5db167074f631eed3e7"
const APIToken = "ATTAbeec5eaa8a7337fab5d2c505dafe1d14c531be3c8247e6802a8ddc185955220cD98D4C3B"


export function getLists(id) {
    return axios.get(`https://api.trello.com/1/boards/${id}/lists?key=${APIKey}&token=${APIToken}`)
        .then(response => response.data)
        .catch(error => console.log("Error occured in fetching Lists" + error))
}

export function createList(id, name) {
    return axios.post(`https://api.trello.com/1/boards/${id}/lists?name=${name}&key=${APIKey}&token=${APIToken}`)
        .then(response => response.data)
        .catch(error => console.log("Error occured in creating Lists" + error))
}

export function archieveList(id) {
    return axios.put(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${APIKey}&token=${APIToken}`)
        .catch(error => console.log("Error occured while deleting list" + error))
}
