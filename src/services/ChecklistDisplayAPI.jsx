import axios from "axios";

const APIKey = "512b806d37d0b5db167074f631eed3e7"
const APIToken = "ATTAbeec5eaa8a7337fab5d2c505dafe1d14c531be3c8247e6802a8ddc185955220cD98D4C3B"

export const getChecklists=(id) =>  {
    return axios.get(`https://api.trello.com/1/cards/${id}/checklists?key=${APIKey}&token=${APIToken}`)
        .then(response => response.data)
       .catch(error => console.log("Error occured while getting checklists "+error))
}

export const createChecklist = (id,checkListName) => {
    return axios.post(`https://api.trello.com/1/checklists?name=${checkListName}&idCard=${id}&key=${APIKey}&token=${APIToken}`)
        .then(response => response.data)
        .catch(error => console.log("Error occured while creating checklists " + error))
}

export const deleteChecklist = (id) => {
    return axios.delete(`https://api.trello.com/1/checklists/${id}?key=${APIKey}&token=${APIToken}`)
        .catch(error => console.log("Error occured while deleting checklists " + error))
}