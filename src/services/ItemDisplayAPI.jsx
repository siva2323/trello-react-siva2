import axios from "axios";

const APIKey = "512b806d37d0b5db167074f631eed3e7"
const APIToken = "ATTAbeec5eaa8a7337fab5d2c505dafe1d14c531be3c8247e6802a8ddc185955220cD98D4C3B"


export const getItems = (checkListId) => {
    return axios.get(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${APIKey}&token=${APIToken}`)
        .then(response => response.data)
        .catch(error => console.log("Error occured in getting items"+error))
}

export function createItem(checkListId, checkItemName) {
    return axios.post(`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemName}&key=${APIKey}&token=${APIToken}`)
        .then(response => response.data)
        .catch(error => console.log("Error occured in creating items " + error))
}

export function deleteItem(checkListId, checkItemId) {
    return axios.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${APIKey}&token=${APIToken}`)
    .catch(error => console.log("error occured in deleting items "+error))
}
