import axios from "axios"

const APIKey = "512b806d37d0b5db167074f631eed3e7"
const APIToken = "ATTAbeec5eaa8a7337fab5d2c505dafe1d14c531be3c8247e6802a8ddc185955220cD98D4C3B"



export function getCards(id) {
    return axios.get(`https://api.trello.com/1/lists/${id}/cards?key=${APIKey}&token=${APIToken}`)
        .then(response => response.data)
        .catch(error => console.log("Error occured in fetching cards" + error))
}

export function deleteCard(id) {
    return axios.delete(`https://api.trello.com/1/cards/${id}?key=${APIKey}&token=${APIToken}`)
        .catch(error => console.log("Error occured in deleting cards" + error))
}

export function createCard(id, name) {
    return axios.post(`https://api.trello.com/1/cards?name=${name}&idList=${id}&key=${APIKey}&token=${APIToken}`)
        .then(response => response.data)
        .catch(error => console.log("Error occured in creating card" + error))
}

